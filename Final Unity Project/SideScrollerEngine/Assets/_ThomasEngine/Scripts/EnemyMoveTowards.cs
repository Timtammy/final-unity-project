﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMoveTowards : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyGO;
    [SerializeField]
    private float speed = 3;
    [SerializeField]
    private float maxArrivalDistance = 2;
    [SerializeField]
    private float maxChaseDistance = 10;
    [SerializeField]
    private float maxHeightDifference = 1;
    [SerializeField]
    private bool ignoreZTurning = true;
    [SerializeField]
    private bool ignoreTargetYPos = true;

    private bool arrived;
    public bool HasArrived () { return arrived; }
    private bool stoppedChasing;
    public bool HasStoppedChasing () { return stoppedChasing; }

    private Enemy en;

    private void Start()
    {
        en = enemyGO.GetComponent<Enemy>();
    }

    public void MoveTowardsTarget (GameObject _target)
    {
        StopAllCoroutines ();
        StartCoroutine (StartMoveTowardsTarget (_target));
    }

    IEnumerator StartMoveTowardsTarget (GameObject _target)
    {
        arrived = false;
        stoppedChasing = false;
        float distance = Vector2.Distance (_target.transform.position, enemyGO.transform.position);
        while (distance > maxArrivalDistance && distance < maxChaseDistance)
        {
            if (!en.IsStunned())
            {
                distance = Vector2.Distance(_target.transform.position, enemyGO.transform.position);
                //stop enemy from turning/moving if the target is directly above or below
                float vDiff = Mathf.Abs(_target.transform.position.y - enemyGO.transform.position.y);
                float hDiff = Mathf.Abs(_target.transform.position.x - enemyGO.transform.position.x);
                bool stop = vDiff > maxHeightDifference && hDiff < maxArrivalDistance;
                if (!stop)
                {
                    MoveToTarget(_target.transform);
                    RotateTowardsTarget(_target.transform);
                }
            }
            //wait one frame and return
            yield return new WaitForFixedUpdate ();
        }
        if (distance < maxArrivalDistance)
        {
            RotateTowardsTarget(_target.transform);
            arrived = true;
        }
        if (distance > maxChaseDistance)
            stoppedChasing = true;

    }

    void MoveToTarget(Transform _target)
    {
        if (ignoreTargetYPos)
            enemyGO.transform.Translate(enemyGO.transform.right * speed * Time.deltaTime, Space.World);
        else
            enemyGO.transform.position = Vector2.MoveTowards(enemyGO.transform.position, _target.position, speed * Time.deltaTime);
    }

    void RotateTowardsTarget(Transform _target)
    {
        if (ignoreZTurning)
            enemyGO.transform.right = new Vector3(_target.position.x, enemyGO.transform.position.y) - enemyGO.transform.position;
        else
            enemyGO.transform.right = _target.position - enemyGO.transform.position;
    }

    public void StopMoveTowardsTarget ()
    {
        StopAllCoroutines ();
    }
}